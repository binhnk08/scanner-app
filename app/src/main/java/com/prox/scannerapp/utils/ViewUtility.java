package com.prox.scannerapp.utils;

import android.view.View;

public class ViewUtility {
    public static void setEnabled(final View button, final boolean enabled) {
        button.setEnabled(enabled);
        button.setAlpha(enabled ? 1f : 0.3f);
    }
}
