package com.prox.scannerapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class SharedPreferencesUtils {
    private static final String SHARED_PREFERENCES_NAME = "APP_SCANNER";

    public static void putInt(Context context, String keyWord, int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(keyWord, value);
        editor.apply();
    }

    public static void putString(Context context, String keyWord, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(keyWord, value);
        editor.apply();
    }

    public static void putFloat(Context context, String keyWord, float value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(keyWord, value);
        editor.apply();
    }

    public static void putBoolean(Context context, String keyWord, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(keyWord, value);
        editor.apply();
    }

    public static void putLong(Context context, String keyWord, long value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(keyWord, value);
        editor.apply();
    }

    public static int getInt(Context context, String keyWord, int defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences != null) return sharedPreferences.getInt(keyWord, defaultValue);
        return defaultValue;
    }

    public static String getString(Context context, String keyWord, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences != null) return sharedPreferences.getString(keyWord, defaultValue);
        return defaultValue;
    }

    public static float getFloat(Context context, String keyWord, float defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences != null) return sharedPreferences.getFloat(keyWord, defaultValue);
        return defaultValue;
    }

    public static boolean getBoolean(Context context, String keyWord, boolean defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences != null) return sharedPreferences.getBoolean(keyWord, defaultValue);
        return false;
    }

    public static long getLong(Context context, String keyWord, long defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences != null) return sharedPreferences.getLong(keyWord, defaultValue);
        return defaultValue;
    }
}
