package com.prox.scannerapp.utils;

public class AppConstant {
    public static class IntentExtra {
        public static final String EXTRA_BITMAP_TAKE_SHOT = "EXTRA_BITMAP_TAKE_SHOT";
    }
}
