package com.prox.scannerapp.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.prox.scannerapp.R;
import com.prox.scannerapp.databinding.ActivityCropperBinding;
import com.prox.scannerapp.utils.AppConstant;
import com.prox.scannerapp.utils.MyCache;
import com.prox.scannerapp.utils.ViewUtility;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;

public class CropperActivity extends AppCompatActivity {

    ActivityCropperBinding binding;
    CropperViewModel cropperViewModel;
    int totalBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.color_app));
        binding = ActivityCropperBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        cropperViewModel = new ViewModelProvider(this).get(CropperViewModel.class);
        Intent intent = getIntent();
        totalBitmap = intent.getIntExtra(AppConstant.IntentExtra.EXTRA_BITMAP_TAKE_SHOT, 0);
        initCropImage();
        cropperViewModel.getCurrentPhoto().observe(this, photo -> {
            binding.tvNumberOfPhoto.setText((photo + 1) + "/" + totalBitmap);
            CropImageView cropImageView = cropperViewModel.getCropViewByIndex(photo);
            binding.frameCrop.removeAllViews();
            binding.frameCrop.addView(cropImageView);
            if (totalBitmap > 1) {
                if (photo == 0) {
                    ViewUtility.setEnabled(binding.btnPreviousPhoto, false);
                    ViewUtility.setEnabled(binding.btnNextPhoto, true);
                } else if (photo == totalBitmap - 1) {
                    ViewUtility.setEnabled(binding.btnPreviousPhoto, true);
                    ViewUtility.setEnabled(binding.btnNextPhoto, false);
                } else {
                    ViewUtility.setEnabled(binding.btnPreviousPhoto, true);
                    ViewUtility.setEnabled(binding.btnNextPhoto, true);
                }
            } else {
                ViewUtility.setEnabled(binding.btnPreviousPhoto, false);
                ViewUtility.setEnabled(binding.btnNextPhoto, false);
                binding.btnDelete.setVisibility(View.GONE);
            }
        });
        initView();
        cropperViewModel.setCurrentPhoto(0);
    }

    private void initView() {
        if (totalBitmap > 1) binding.btnDelete.setVisibility(View.VISIBLE);
        else binding.btnDelete.setVisibility(View.GONE);

        binding.btnPreviousPhoto.setOnClickListener(v -> {
            Integer currentPhoto = cropperViewModel.getCurrentPhoto().getValue();
            if (currentPhoto != null && currentPhoto > 0) {
                currentPhoto--;
                cropperViewModel.setCurrentPhoto(currentPhoto);
            }
        });
        binding.btnNextPhoto.setOnClickListener(v -> {
            Integer currentPhoto = cropperViewModel.getCurrentPhoto().getValue();
            if (currentPhoto != null && currentPhoto < totalBitmap - 1) {
                currentPhoto++;
                cropperViewModel.setCurrentPhoto(currentPhoto);
            }
        });

        binding.btnDelete.setOnClickListener(v -> {
            Integer currentPhoto = cropperViewModel.getCurrentPhoto().getValue();
            if (currentPhoto != null) {
                List<CropImageView> list = cropperViewModel.getListCropImage().getValue();
                if (list != null) {
                    list.remove((int) currentPhoto);
                    if (currentPhoto >= list.size()) {
                        currentPhoto = list.size() - 1;
                    }
                    cropperViewModel.setListCropImage(list);
                    totalBitmap = list.size();
                }
                cropperViewModel.setCurrentPhoto(currentPhoto);
            }
        });

        binding.btnBack.setOnClickListener(v -> finish());

        binding.btnRotateLeft.setOnClickListener(v -> {
            Integer currentPhoto = cropperViewModel.getCurrentPhoto().getValue();
            if (currentPhoto != null) {
                CropImageView cropImageView = cropperViewModel.getCropViewByIndex(currentPhoto);
                cropImageView.rotateImage(90);
            }
        });

        binding.btnRotateRight.setOnClickListener(v -> {
            Integer currentPhoto = cropperViewModel.getCurrentPhoto().getValue();
            if (currentPhoto != null) {
                CropImageView cropImageView = cropperViewModel.getCropViewByIndex(currentPhoto);
                cropImageView.rotateImage(- 90);
            }
        });
    }

    private void initCropImage() {
        List<CropImageView> list = new ArrayList<>();
        for (int i = 0; i < totalBitmap; i++) {
            CropImageView cropImageView = new CropImageView(this);
            Bitmap bitmap = MyCache.getInstance().retrieveBitmapFromCache("BITMAP_CACHE_" + i);
            cropImageView.setImageBitmap(bitmap);
            list.add(cropImageView);
        }
        cropperViewModel.setListCropImage(list);
    }
}