package com.prox.scannerapp.ui.activities;

import androidx.camera.core.ImageCapture;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ScannerViewModel extends ViewModel {
    private final MutableLiveData<Integer> totalBitmap;
    private final MutableLiveData<Integer> flashMode;
    private final MutableLiveData<Boolean> gridMode;
    private final MutableLiveData<Boolean> autoManual;

    public ScannerViewModel() {
        totalBitmap = new MutableLiveData<>();
        flashMode = new MutableLiveData<>();
        gridMode = new MutableLiveData<>();
        autoManual = new MutableLiveData<>();
    }

    public LiveData<Integer> getTotalBitmap() {
        return totalBitmap;
    }

    public void setTotalBitmap(int total) {
        totalBitmap.setValue(total);
    }

    public LiveData<Integer> getFlashMode() {
        return flashMode;
    }

    public void changeFlashMode() {
        Integer mode = flashMode.getValue();
        if (mode == null) mode = ImageCapture.FLASH_MODE_OFF;
        switch (mode) {
            case ImageCapture.FLASH_MODE_OFF:
                flashMode.setValue(ImageCapture.FLASH_MODE_AUTO);
                break;
            case ImageCapture.FLASH_MODE_AUTO:
                flashMode.setValue(ImageCapture.FLASH_MODE_ON);
                break;
            case ImageCapture.FLASH_MODE_ON:
                flashMode.setValue(ImageCapture.FLASH_MODE_OFF);
                break;
        }
    }

    public LiveData<Boolean> getGridMode() {
        return gridMode;
    }

    public void changeGridMode() {
        Boolean mode = gridMode.getValue();
        if (mode == null) mode = false;
        gridMode.setValue(!mode);
    }

    public LiveData<Boolean> getAutoManual() {
        return autoManual;
    }

    public void changeAutoManual() {
        Boolean mode = autoManual.getValue();
        if (mode == null) mode = true;
        autoManual.setValue(!mode);
    }
}
