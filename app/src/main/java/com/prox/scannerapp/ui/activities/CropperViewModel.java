package com.prox.scannerapp.ui.activities;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

public class CropperViewModel extends ViewModel {
    private final MutableLiveData<Integer> currentPhoto;
    private final MutableLiveData<List<CropImageView>> listCropImage;

    public CropperViewModel() {
        currentPhoto = new MutableLiveData<>();
        listCropImage = new MutableLiveData<>();
    }

    public LiveData<Integer> getCurrentPhoto() {
        return currentPhoto;
    }

    public void setCurrentPhoto(int photo) {
        currentPhoto.setValue(photo);
    }

    public LiveData<List<CropImageView>> getListCropImage() {
        return listCropImage;
    }

    public void setListCropImage(List<CropImageView> list) {
        listCropImage.setValue(list);
    }

    public CropImageView getCropViewByIndex(int index) {
        List<CropImageView> list = listCropImage.getValue();
        if (list != null && index < list.size()) {
            return list.get(index);
        }
        return null;
    }
}
