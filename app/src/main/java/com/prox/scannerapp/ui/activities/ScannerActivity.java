package com.prox.scannerapp.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.google.common.util.concurrent.ListenableFuture;
import com.prox.scannerapp.R;
import com.prox.scannerapp.databinding.ActivityScannerBinding;
import com.prox.scannerapp.utils.AppConstant;
import com.prox.scannerapp.utils.MyCache;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;

public class ScannerActivity extends AppCompatActivity {

    private ActivityScannerBinding binding;
    private ScannerViewModel scannerViewModel;
    private ImageCapture imageCapture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.color_app));
        binding = ActivityScannerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        scannerViewModel = new ViewModelProvider(this).get(ScannerViewModel.class);
        scannerViewModel.getTotalBitmap().observe(this, total -> {
            if (total > 0) {
                binding.tvNumberPhotoRecently.setVisibility(View.VISIBLE);
                binding.layoutRecentlyPhoto.setVisibility(View.VISIBLE);
                binding.btnNext.setVisibility(View.VISIBLE);
                binding.tvNumberPhotoRecently.setText(String.valueOf(total));
            }
        });
        scannerViewModel.getFlashMode().observe(this, mode -> {
            switch (mode) {
                case ImageCapture.FLASH_MODE_OFF:
                    binding.btnFlash.setImageResource(R.drawable.ic_scanner_flash_off);
                    break;
                case ImageCapture.FLASH_MODE_AUTO:
                    binding.btnFlash.setImageResource(R.drawable.ic_scanner_flash_auto);
                    break;
                case ImageCapture.FLASH_MODE_ON:
                    binding.btnFlash.setImageResource(R.drawable.ic_scanner_flash_on);
                    break;
            }
        });
        scannerViewModel.getGridMode().observe(this, turnOn -> {
            binding.btnGrid.setImageResource(turnOn ? R.drawable.ic_scanner_grid_on : R.drawable.ic_scanner_grid_off);
            binding.layoutGrid.setVisibility(turnOn ? View.VISIBLE : View.GONE);
        });
        scannerViewModel.getAutoManual().observe(this, autoManual -> {
            if (autoManual) {
                binding.tvAutoManual.setText(R.string.auto);
            } else {
                binding.tvAutoManual.setText(R.string.manual);
            }
        });
        startCamera();
        initView();
    }

    private void initView() {
        binding.btnBack.setOnClickListener(v -> finish());
        binding.btnCapture.setOnClickListener(v -> takePhoto());
        binding.btnFlash.setOnClickListener(v -> scannerViewModel.changeFlashMode());
        binding.btnGrid.setOnClickListener(v -> scannerViewModel.changeGridMode());
        binding.tvAutoManual.setOnClickListener(v -> scannerViewModel.changeAutoManual());
        binding.layoutRecentlyPhoto.setOnClickListener(v -> openCropper());
        binding.btnNext.setOnClickListener(v -> openCropper());
    }

    private void openCropper() {
        Intent intent = new Intent(this, CropperActivity.class);
        intent.putExtra(AppConstant.IntentExtra.EXTRA_BITMAP_TAKE_SHOT, scannerViewModel.getTotalBitmap().getValue());
        startActivity(intent);
    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                Preview preview = new Preview.Builder().build();
                imageCapture = new ImageCapture.Builder().build();
                CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture);
                preview.setSurfaceProvider(binding.viewFinder.getSurfaceProvider());
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }, ContextCompat.getMainExecutor(this));
    }

    private void takePhoto() {
        Log.d("binhnk08", "imageCapture = " + imageCapture);
        if (imageCapture == null) return;
        binding.btnCapture.setClickable(false);
        binding.loading.setVisibility(View.VISIBLE);
        int flashMode = ImageCapture.FLASH_MODE_OFF;
        if (scannerViewModel.getFlashMode().getValue() != null)
            flashMode = scannerViewModel.getFlashMode().getValue();
        imageCapture.setFlashMode(flashMode);

        imageCapture.takePicture(ContextCompat.getMainExecutor(this), new ImageCapture.OnImageCapturedCallback() {
            @Override
            public void onCaptureSuccess(@NonNull @NotNull ImageProxy image) {
                Bitmap bitmap = imageProxyToBitmap(image);
                image.close();
                Glide.with(ScannerActivity.this).load(bitmap).into(binding.photoRecently);
                Integer total = scannerViewModel.getTotalBitmap().getValue();
                if (total == null) total = 0;
                MyCache.getInstance().getLru().put("BITMAP_CACHE_" + total, bitmap);
                scannerViewModel.setTotalBitmap(total + 1);
                binding.btnCapture.setClickable(true);
                binding.loading.setVisibility(View.GONE);
            }

            @Override
            public void onError(@NonNull @NotNull ImageCaptureException exception) {
                super.onError(exception);
                binding.btnCapture.setClickable(true);
                binding.loading.setVisibility(View.GONE);
            }
        });
    }

    private Bitmap imageProxyToBitmap(ImageProxy image) {
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
//        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
        int rotation = image.getImageInfo().getRotationDegrees();
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}